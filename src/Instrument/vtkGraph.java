package Instrument;

/**
 * Created by Sabatu on 4/10/2017.
 */
import vtk.*;
import vtk.vtkGraph.*;

import java.util.ArrayList;

/*
 * Java language example to select edges and vertices from a generated Graph.
 */
public class vtkGraph {

    /**
     * All Java programs require a host class. in addition,
     * these two instance variables provide access to the
     * callback data used in this example.
     */
    vtkGraphLayoutView view;
    vtkAnnotationLink link;
    static vtkGraphLayoutView view2;
    static vtkAnnotationLink link2;
    /*
     * The following static calls will load the respective
     * vtkJava interface libraries on first reference to this
     * class.
     */
 /*  static {
        System.loadLibrary("vtkCommonJava");
        System.loadLibrary("vtkFilteringJava");
        System.loadLibrary("vtkViewsJava");
        System.loadLibrary("vtkImagingJava");
        System.loadLibrary("vtkInfovisJava");
        System.loadLibrary("vtkGraphicsJava");
        System.loadLibrary("vtkRenderingJava");

   }

*/


    static {
        if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
            for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
                if (!lib.IsLoaded())
                    System.out.println(lib.GetLibraryName() + " not loaded");
            }
            System.out.println("Make sure the search path is correct: ");
            System.out.println(System.getProperty("java.library.path"));
        }
        vtkNativeLibrary.DisableOutputWindow(null);
    }

    /*
     * primary test driver, creates an instance of this class
     * and then runs the example function.

     */
    public static void main(String[] args) {

        //vtkGraph me = new vtkGraph();
        //me.doit();

        vtkGraph me2 = new vtkGraph();


        me2.doit2();






    }



    /*
     * doit creates local objects and instantiates instance variables
     */
public void doit2(){

    vtkMutableDirectedGraph a = new vtkMutableDirectedGraph ();

    vtkIntArray vertId = new vtkIntArray();

    vtkStringArray labels = new vtkStringArray();

    labels.SetName("Label");

    vertId.SetName("id");

    a.GetVertexData().AddArray(vertId);

    int v;

    int[] vector = new int[10];


    vtkPoints points = new vtkPoints();


    int x = 0;
    int y = 0;
    int z = 0;

    for (v = 0; v < 10; v++)
    {
        vector[v] =  a.AddVertex();

        vertId.InsertNextValue(v);

        labels.InsertValue(v, "label");

        points.InsertNextPoint(x, y, z);



        //No noticeable difference..
       x += 20;


        //Distance from Camera (looks like scaling perhaps)
       // y+=20;

        //Distance from each other
       //z+=20;
    }

    a.SetPoints(points);



    a.GetVertexData().AddArray(labels);

    //Dynamically Generate Edges
    for (int e = 0; e < 10; ++e)
    {
        //a.AddGraphEdge(e, (e+1)%10);

        a.AddGraphEdge(e, (e+1)%10);


    }

    vtkGraphLayoutView d = new vtkGraphLayoutView();

    d.SetEdgeLabelFontSize(18);

    d.SetEdgeLabelVisibility(true);

    d.SetVertexLabelVisibility(true);

    //This turns on some sort of coloration bar
    d.SetEdgeScalarBarVisibility(false);

    //Nothing noticeable

    d.SetColorEdges(true);

    d.SetEdgeSelection(true);

    d.SetIconVisibility(true);

        //Create an integer array to store vertex id data & link it with its degree value as a scalar.
    //vtkIntArray degree  = new vtkIntArray();
    //degree.SetNumberOfComponents(1);
  //  degree.SetName("degree");
  //  degree.SetNumberOfTuples(7);
   // degree.SetValue(0,2);
    //degree.SetValue(1,1);
  //  degree.SetValue(2,3);
   // degree.SetValue(3,3);
   // degree.SetValue(4,4);
   // degree.SetValue(5,2);
   // degree.SetValue(6,1);



   //vtkUnstructuredGrid G = new vtkUnstructuredGrid();
  // G.GetPointData().SetScalars(degree);
  // G.SetPoints(points);


  // d.SetLayoutStrategyToClustering2D();
   d.SetLayoutStrategyToFast2D();
   //d.SetLayoutStrategyToCommunity2D();
  // d.SetLayoutStrategyToSimple2D();
  // d.SetLayoutStrategyToSpanTree();

    d.AddRepresentationFromInput(a);

    vtkInteractorStyleJoystickCamera e = new vtkInteractorStyleJoystickCamera();

    d.SetInteractorStyle(e);

    d.SetGlyphType(7);



    d.ResetCamera();
    d.Render();

    d.GetLayoutStrategy().SetReferenceCount(1);

    d.GetInteractor().Start();

















    view2 = new vtkGraphLayoutView();

    //view.AddRepresentationFromInputConnection(source.GetOutputPort());

    view2.AddRepresentationFromInput(a.GetData(a.GetInformation()));



    view2.Update();
    view2.UpdateLayout();


    vtkDataRepresentation rep = view2.GetRepresentation(0);

		/*
		* The vtkDataRepresentation should already have a vtkAnnotationLink,
		* so we just want to grab it and add an observer with our callback function attached
		* Note that Java callbacks use the this "pointer" to anchor vtk objects used by the callback
		* to establish references to required vtkobjects.
		*/
    link2 = rep.GetAnnotationLink();
    link2.AddObserver("AnnotationChangedEvent", this,  "selectionCallback");



   // view2.GetRenderWindow().SetSize(600, 600);
    //view2.ResetCamera();
    //view2.Render();
   // view2.GetInteractor().Start();




}




    //vtkPoints points = new vtkPoints();

    //points.InsertNextPoint(0.0, 0.0, 0.0);
    // points.InsertNextPoint(1.0, 0.0, 0.0);
    // points.InsertNextPoint(0.0, 1.0, 0.0);
    //points.InsertNextPoint(0.0, 0.0, 2.0);

    // a.SetPoints(points);


    public void doit() {

        vtkRandomGraphSource source = new vtkRandomGraphSource();

        source.SetNumberOfVertices(20);
        source.SetNumberOfEdges(40);






        source.Update();



        view =  new vtkGraphLayoutView();





        view.AddRepresentationFromInputConnection(source.GetOutputPort());


		/*
		 * This was described as vtkRenderedGraphRepresentation in the python example,
		 * but the java type returns it as vtkDataRepresentation.
		 *
		 */

        vtkDataRepresentation rep = view.GetRepresentation(0);

		/*
		* The vtkDataRepresentation should already have a vtkAnnotationLink,
		* so we just want to grab it and add an observer with our callback function attached
		* Note that Java callbacks use the this "pointer" to anchor vtk objects used by the callback
		* to establish references to required vtkobjects.
		*/
        link = rep.GetAnnotationLink();
        link.AddObserver("AnnotationChangedEvent", this,  "selectionCallback");


        view.GetRenderWindow().SetSize(600, 600);
        view.ResetCamera();
        view.Render();
        view.GetInteractor().Start();

    }
    /*
     * The Java callback signature has no parameters. This complicates some
     * vtk processes by eliminating the eventid and user data normally available
     * to C++ logic.
     */
    public void  selectionCallback() {

        // In C++ there is some extra data passed to the callback, but in Python
        // the callback data is lost...

        // There can be two selection nodes, but which one is vertices and which is edges
        // does not seem to be guaranteed...

        System.out.println("in selection callback");

        vtkSelection sel = link.GetCurrentSelection();
        vtkSelectionNode node0 = sel.GetNode(0);
        int node0_field_type = node0.GetFieldType();







        vtkIdTypeArray sel_list0 = (vtkIdTypeArray)(link.GetCurrentSelection().GetNode(0).GetSelectionList());


        vtkSelectionNode node1 = sel.GetNode(1);
        int node1_field_type = node1.GetFieldType();

        vtkIdTypeArray sel_list1 = (vtkIdTypeArray)(link.GetCurrentSelection().GetNode(1).GetSelectionList());

        if (sel_list0.GetNumberOfTuples() > 0) {
            printFieldType(node0_field_type);
            for (int ii = 0; ii < sel_list0.GetNumberOfTuples(); ii++){
                System.out.print( "\t" + sel_list0.GetValue(ii));
            }
            System.out.println(" - on list 0." );
        }

        //This segment is used in the labeling scheme for the output

            if (sel_list1.GetNumberOfTuples() > 0) {
            printFieldType(node1_field_type);
            for (int ii = 0; ii < sel_list1.GetNumberOfTuples(); ii++){
                System.out.print( "\t" + sel_list1.GetValue(ii));
            }
            System.out.println(" - on list 1." );
        }
        System.out.println( "- - -");

    }

    public void printFieldType(int field_type) {
        if (field_type == 3)
            System.out.print("Vertices Selected:");
        else if (field_type == 4)
            System.out.print ("Edges Selected:");
        else
            System.out.print ("Unknown type:");
    }

}