package Instrument;

import org.jgrapht.ListenableGraph;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;

/**
 * Created by Sabatu on 4/5/2017.
 */
public class readLogs {

    public static void main(String[] args) throws Exception {

        File startDir = new File("C:\\Users\\Sabatu\\AppData\\Local\\Android\\sdk\\platform-tools\\");

        String startDir2 = startDir.toString();

        //C:\Users\Sabatu\AppData\Local\Android\sdk\platform-tools

        //String startDir = System.getProperty("user.dir"); // start in current dir (change if needed)

        //ProcessBuilder pb = new ProcessBuilder("adb","logcat","-d");
        ProcessBuilder pb = new ProcessBuilder("C:\\Users\\Sabatu\\AppData\\Local\\Android\\sdk\\platform-tools\\adb.exe","logcat");





        pb.directory(new File(startDir2));  // start directory
        pb.redirectErrorStream(true); // redirect the error stream to stdout
        Process p = pb.start(); // start the process
        // start a new thread to handle the stream input

        new Thread(new ProcessTestRunnable(p)).start();

        p.waitFor();  // wait if needed
    }








    // mimics stream gobbler, but allows user to process the result
    static class ProcessTestRunnable implements Runnable {

        private static final Dimension DEFAULT_SIZE = new Dimension( 1240, 750 );

        Process p;
        BufferedReader br;

        ProcessTestRunnable(Process p) {
            this.p = p;
        }

        String vertexDescription = new String();

        public IntentType[] capturedIntent = new IntentType[300];
        public int intentCounter = 0;

        public void run() {

            try {


                InputStreamReader isr = new InputStreamReader(p.getInputStream());

                br = new BufferedReader(isr);

                String line = null;
                int lineNumber = 0;

                //private static final Dimension DEFAULT_SIZE = new Dimension( X: 530, Y: 320 );


                int sentX = 20;
                int sentY = 30;
                int recvX = 20;
                int recvY = 130;

                int compType = 0;

                int intentCat  = 0;

                //This is a switch used inside the loop, when evaluated to true it activates a slew of additional if conditionals that filter logcat
                //for additional information
                boolean captureIntentInfo = false;

                //This switch is used to condition output sent to the graphing utility
                boolean sentIntent = false;



                /*
                Flipper alpha = new Flipper();


                */

                dynCGvisualizer alpha = new dynCGvisualizer();

                JFrame frame = new JFrame();
                frame.getContentPane().add(alpha);

                frame.resize(DEFAULT_SIZE );
                frame.setVisible(true);

                String theSeparator = System.lineSeparator();


                //Dynamically reads from file within this loop
                while ((line = br.readLine()) != null) {

                    Integer logcatLineNumber = lineNumber;



                    //Looks for a new intent
                    if (line.contains("INTENT")) {



                        System.out.println("It found an intent somewhere...");






                        if(line.contains("sent"))
                        {

                            //logic for a captured send intent

                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());



                            capturedIntent[intentCounter] = new IntentType(logcatLineNumber.toString());

                            capturedIntent[intentCounter].setType("Sent");

                            capturedIntent[intentCounter].setTimestamp(timeStamp);

                            sentIntent = true;

                            intentCat = 1;

                            captureIntentInfo = true;

                        }

                        else if(line.contains("Received"))
                        {

                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());



                            capturedIntent[intentCounter] = new IntentType(logcatLineNumber.toString());

                            capturedIntent[intentCounter].setType("Received");

                            capturedIntent[intentCounter].setTimestamp(timeStamp);



                            intentCat = 2;

                            captureIntentInfo = true;





                        }
                        else
                        {


                        }


                    }

                    //Capture and process additional intent information for visualization
                    if(captureIntentInfo)
                    {
                        if(line.contains("CALLER:")) {

                            compType = line.indexOf(": :");
                            String newline = line.substring(compType + 1);

                            capturedIntent[intentCounter].setCaller(newline);

                        }

                        if(line.contains("CALLSITE"))
                        {

                            compType = line.indexOf(": :");
                            String newline = line.substring(compType + 1);

                            capturedIntent[intentCounter].setCallsite(newline);
                        }

                        if(line.contains("Action=") && !line.contains("null"))
                    {

                        compType = line.indexOf("=");
                        String newline = line.substring(compType + 1);

                        capturedIntent[intentCounter].setAction(newline);
                    }

                        if(line.contains("PackageName=") && !line.contains("null"))
                        {

                            compType = line.indexOf("=");
                            String newline = line.substring(compType + 1);

                            capturedIntent[intentCounter].setPackageName(newline);
                        }

                        if(line.contains("DataString=") && !line.contains("null"))
                        {

                            compType = line.indexOf("=");
                            String newline = line.substring(compType + 1);

                            capturedIntent[intentCounter].setdataString(newline);
                        }

                        if(line.contains("DataURI=") && !line.contains("null"))
                        {

                            compType = line.indexOf("=");
                            String newline = line.substring(compType + 1);

                            capturedIntent[intentCounter].setdataURI(newline);
                        }

                        if(line.contains("Scheme=") && !line.contains("null"))
                        {

                            compType = line.indexOf("=");
                            String newline = line.substring(compType + 1);

                            capturedIntent[intentCounter].setScheme(newline);
                        }

                        if(line.contains("Flags=") && !line.contains("null"))
                        {

                            compType = line.indexOf("=");
                            String newline = line.substring(compType + 1);

                            capturedIntent[intentCounter].setFlags(newline);
                        }

                        if(line.contains("Type=") && !line.contains("null"))
                        {

                            compType = line.indexOf("=");
                            String newline = line.substring(compType + 1);

                            capturedIntent[intentCounter].setType(newline);
                        }

                        if(line.contains("Extras=") && !line.contains("null"))
                        {

                            compType = line.indexOf("=");
                            String newline = line.substring(compType + 1);

                            capturedIntent[intentCounter].setExtras(newline);
                        }

                        if(line.contains("Component=") && !line.contains("null"))
                        {

                            compType = line.indexOf("=");
                            String newline = line.substring(compType + 1);

                            capturedIntent[intentCounter].setComponent(newline);


                        }

                        if(line.contains("Component="))
                        {

                            if(intentCat == 1)
                            {
                                alpha.updateGraph(capturedIntent, intentCounter, sentX, sentY);
                                intentCounter += 1;


                                sentX += 160;

                            }
                            else if(intentCat == 2)
                            {
                                alpha.updateGraph(capturedIntent, intentCounter, recvX, recvY);
                                intentCounter += 1;


                                recvX += 160;

                            }

                            intentCat = 0;

                            captureIntentInfo = false;


                        }










                        }


                    System.out.println("LOGGED OUTPUT LINE " + lineNumber + ": " + line);
                    lineNumber++;

                    // do something with the output here...

                }
            }catch (IOException ex) {ex.printStackTrace();
            }
        } //<====run()
    }
}