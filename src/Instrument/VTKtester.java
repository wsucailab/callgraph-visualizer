
package Instrument;




import vtk.*;


// Then we define our class.
public class VTKtester {

   // In the static contructor we load in the native code.


   // The libraries must be in your path to work.



  /* static {

       System.loadLibrary("vtkCommonCoreJava");
       System.loadLibrary("vtkFiltersSourcesJava");
       System.loadLibrary("vtkInteractionStyleJava");
       System.loadLibrary("vtkRenderingOpenGL2Java");
   }

*/

    static {
        if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
            for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
                if (!lib.IsLoaded())
                    System.out.println(lib.GetLibraryName() + " not loaded");
            }
            System.out.println("Make sure the search path is correct: ");
            System.out.println(System.getProperty("java.library.path"));
        }
        vtkNativeLibrary.DisableOutputWindow(null);
    }
   // now the main program
   public static void main (String []args) {
       vtk.vtkCylinderSource cylinder = new vtk.vtkCylinderSource();
       cylinder.SetResolution( 8 );

       vtk.vtkPolyDataMapper cylinderMapper = new vtk.vtkPolyDataMapper();
       cylinderMapper.SetInputConnection( cylinder.GetOutputPort() );

       vtk.vtkActor cylinderActor = new vtk.vtkActor();
       cylinderActor.SetMapper( cylinderMapper );
       cylinderActor.GetProperty().SetColor(1.0000, 0.3882, 0.2784);
       cylinderActor.RotateX(30.0);
       cylinderActor.RotateY(-45.0);

       vtkRenderer ren = new vtkRenderer();
       vtkRenderWindow renWin = new vtkRenderWindow();
       renWin.AddRenderer( ren );
       ren.AddActor( cylinderActor );

       vtkRenderWindowInteractor iren = new vtkRenderWindowInteractor();
       iren.SetRenderWindow(renWin);

       ren.AddActor(cylinderActor);
       ren.SetBackground(0.1, 0.2, 0.4);
       renWin.SetSize(200, 200);

       iren.Initialize();

       ren.ResetCamera();
       ren.GetActiveCamera().Zoom(1.5);
       renWin.Render();

       iren.Start();
   }
}
