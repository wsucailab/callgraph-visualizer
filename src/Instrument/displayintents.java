package Instrument;


import soot.*;
import soot.jimple.*;
import soot.options.Options;

import java.util.*;






public class displayintents extends SceneTransformer {

	protected  SootClass output = null;
	protected SootMethod mSendCalling = null;
	protected SootMethod mReceiveCalling = null;
	protected SootMethod mSendPassed = null;
	protected SootMethod mReceivePassed = null;
	protected SootMethod mBuildCalling = null;
	protected SootMethod mBuildPassed = null;
	protected SootMethod mSuppCall = null;
	protected SootMethod mSuppInv = null;
	protected SootMethod mGetIntentInfo = null;
	protected SootMethod mPrintRawStmt = null;


	public static void main(String[] args) {

          /* check the arguments */
		if (args.length == 0) {
			System.err.println("Usage: java Driver [options] classname");
			System.exit(0);
		}

		//prefer Android APK files// -src-prec apk
		Options.v().set_src_prec(Options.src_prec_apk);

		System.err.println("Main in Driver ran");

		//output as APK, too//-StatInvCompStr J
		Options.v().set_output_format(Options.output_format_dex);

		// resolve the PrintStream and System soot-classes
		Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
		Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);

		Scene.v().addBasicClass("Instrument.output");

		//Okay, THIS is calling the methods internal transform.
		PackManager.v().getPack("wjtp").add(new Transform("wjtp.mt", new displayintents()));



          /* Give control to Soot to process all options,
           * InvokeStaticInstrumenter.internalTransform will get called.
           */
		soot.Main.main(args);
	}



	@Override
	protected void internalTransform(String phaseName, @SuppressWarnings("rawtypes") Map options) {

		String newLine = System.getProperty("line.separator");

		output  = Scene.v().getSootClass("Instrument.output");

		output.setApplicationClass();

		Scene.v().loadNecessaryClasses();

		int i = output.getMethodCount();

		System.out.println("I Found " + i + " methods in output...");

		mSendCalling = output.getMethodByName("onSendCallingIntent");
		mReceiveCalling = output.getMethodByName("onRecvCallingIntent");
		mSendPassed = output.getMethodByName("onSendPassedIntent");
		mReceivePassed = output.getMethodByName("onRecvPassedIntent");
		mBuildCalling = output.getMethodByName("onBuildInvokedIntent");
		mBuildPassed = output.getMethodByName("onBuildPassedIntent");
		mSuppCall = output.getMethodByName("supplementaryCall");
		mSuppInv = output.getMethodByName("supplementaryInvoke");
		mGetIntentInfo = output.getMethodByName("dumpIntentInfo");
		mPrintRawStmt = output.getMethodByName("printrawStmt");


		Iterator<SootClass> clsIt = Scene.v().getClasses().snapshotIterator();

		while (clsIt.hasNext()) {
			SootClass sClass = clsIt.next();

			if ( sClass.isPhantom() ) {
				// skip phantom classes
				continue;
			}

			if (sClass.isInterface()) continue;

			if (sClass.isInnerClass()) continue;

			if ( !sClass.isApplicationClass() ) {
				// skip library classes
				continue;
			}


                    /* traverse all methods of the class */
			Iterator<SootMethod> meIt = sClass.getMethods().iterator();



			while (meIt.hasNext()) {

				SootMethod sMethod = meIt.next();


				//System.out.println("\n method visited - " + sMethod );

				if ( !sMethod.isConcrete() ) {

					// skip abstract methods and phantom methods, and native methods as well
					continue;
				}

				if ( sMethod.toString().indexOf(": java.lang.Class class$") != -1 ) {
					// don't handle reflections now either
					continue;
				}


				// cannot instrument method event for a method without active body
				//if ( !sMethod.hasActiveBody() ) {
				 //   continue;
				// }



				 Body body = sMethod.retrieveActiveBody();


				 PatchingChain<Unit> pchn = body.getUnits();

				Iterator<Unit> uiter = pchn.snapshotIterator();


				/*Iterates through the statements of the currently selected method. If an intent is found, the foundIntent boolean ensures we dont
				iterate through the body multiple times, since processIntent() will already back up to the beginning of the body and process the entire body of statements.
				 */
				while (uiter.hasNext()) {


					Unit currentUnit = uiter.next();


					Stmt stmtCast = (Stmt)currentUnit;

					String compStmt = stmtCast.toString();


					if(!stmtCast.containsInvokeExpr())
					{
						continue;
					}



					InvokeExpr inv = null;
					try {
						inv = stmtCast.getInvokeExpr();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}





					for (int idx = 0; idx < inv.getArgCount(); idx++) {



						Value curarg = null;
						try {
							curarg = inv.getArg(idx);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}




						if (curarg.getType().equals(Scene.v().getRefType("android.content.Intent"))) {

							/*Setting these two ensures we don't iterate back through the rest of the body after the intent has been processed by processIntent
							(processIntent() rewinds to the beginning of the body, and processes each statement already.)
							 */
							//idx = inv.getArgCount();


							List<Object> itnProbes = new ArrayList<Object>();
							List intentArgs = new ArrayList();

								intentArgs.add(utilities.makeBoxedValue(sMethod, curarg, itnProbes));

								if (intentArgs.isEmpty()) {

									continue;
								}

								String strMethod = sMethod.toString();

								SootClass comparisonClass = sMethod.getDeclaringClass();

							String compType = FindComponentType.getComponentType(comparisonClass);

								String strStmt = stmtCast.toString();

								intentArgs.add(StringConstant.v(strMethod));

								intentArgs.add(StringConstant.v(strStmt));

								intentArgs.add(StringConstant.v(compType));


							String intentSent = "Debug";
							if(FindComponentType.is_IntentReceivingAPI(stmtCast)) {


								intentSent = "Intent received.";
							}
							else if(FindComponentType.is_IntentSendingAPI(stmtCast))
							{
								 intentSent = "Intent sent.";
							}
							else
							{
								 intentSent = "Intent Received.";
							}

							intentArgs.add(StringConstant.v(intentSent));


							Stmt sitnCall = null;
							try {
								sitnCall = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(mGetIntentInfo.makeRef(), intentArgs));
								itnProbes.add(sitnCall);
							} catch (Exception e) {
								e.printStackTrace();
							}

							try {

								for(Object instrumentedCode: itnProbes) {
									pchn.insertBeforeNoRedirect((Unit)instrumentedCode, currentUnit);
								}

							} catch (Exception e) {
								e.printStackTrace();
							}


							processIntent(body,intentSent, mBuildCalling, mBuildPassed, mSuppCall, mSuppInv, strMethod, strStmt, mPrintRawStmt);

							body.validate();

							intentArgs.clear();

								break;



							} //< if filter for finding android.content.Intent within statements






					} //<====conditional loop for statement parsing
				} //<====while loop for statement iteration
			} //<====while loop for method iteration
		} //<===while loop for class iteration
	} //<===internalTransform





	protected static void processIntent(Body body, String intentTypeFound, SootMethod outputCall, SootMethod outputPassed, SootMethod suppCall, SootMethod suppInvoke, String method, String statement, SootMethod printrawSt)
	{



		//generates the String list that will be passed to the output function
		List processedIntent = Collections.synchronizedList(new ArrayList());





	//Stores any unexpandedVariables in the captured invoke statements
		//List<String> expVars = new ArrayList<String>();



		//reset the patching chain to the first unit, so the body can be completely processed.

		PatchingChain pchn2 = body.getUnits();
		Iterator<Unit> uiter2 = pchn2.snapshotIterator();

		while(uiter2.hasNext())
		{
			final Unit curUnit = uiter2.next();

			final String compStmt = curUnit.toString();




			CharSequence VInvCompStr = "virtualinvoke";
			CharSequence SInvCompStr = "specialinvoke";
			CharSequence StatInvCompStr = "staticinvoke";
			CharSequence IntCompStr = "android.content.Intent:";
			CharSequence dumpIntents = "Instrument.output";
			CharSequence supplCalls = "supplementaryCall";


			CharSequence passedIntentComp = "android.content.Intent,";

			//Filters instance calls made by android.content.Intent

			if((compStmt.contains(VInvCompStr) || compStmt.contains(SInvCompStr) || compStmt.contains(StatInvCompStr)) && compStmt.contains(IntCompStr) && !compStmt.contains(dumpIntents) && !compStmt.contains(supplCalls) && !compStmt.equals(method) &&  !compStmt.equals(statement))
			{


				String matchStmt = null;

				try {
					int indexOfChar = compStmt.indexOf("<");
					int CompStmtLength = compStmt.length();
					matchStmt = compStmt.substring(indexOfChar, CompStmtLength);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if(!compStmt.isEmpty()) {

					processedIntent.add(StringConstant.v(matchStmt));

				}

				System.out.println("Processed intent is at size: " + processedIntent.size());


				if(processedIntent.size() == 1) {

					Stmt sitnCall = null;
					try {
						sitnCall = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(outputCall.makeRef(), processedIntent));
					} catch (Exception e) {
						e.printStackTrace();
					}

					//Empty the list before the loop begins processing the next statement
					processedIntent.clear();


					try {
						pchn2.insertBeforeNoRedirect(sitnCall, curUnit);
					} catch (Exception e) {
						e.printStackTrace();
					}

					body.validate();
				}

				else
				{
					processedIntent.clear();

				}




			} //<===end of if conditional searching for intent-invoked instances


			//Filters android.content.Intent being passed as an argument to a function


			else if((compStmt.contains(VInvCompStr) || compStmt.contains(SInvCompStr) || compStmt.contains(StatInvCompStr)) && compStmt.contains(passedIntentComp)  && !compStmt.contains(dumpIntents) && !compStmt.contains(supplCalls) && !compStmt.contains(method) &&  !compStmt.contains(statement))
			{


				String matchStmt = null;
				try {
					int indexOfChar = compStmt.indexOf("<");
					int CompStmtLength = compStmt.length();
					matchStmt = compStmt.substring(indexOfChar, CompStmtLength);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if(!matchStmt.isEmpty()) {
					processedIntent.add(StringConstant.v(matchStmt));

				}


				System.out.println("Processed intent is at size: " + processedIntent.size());

				if(processedIntent.size() == 1) {

					Stmt sitnCall = null;
					try {
						sitnCall = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(outputPassed.makeRef(), processedIntent));
					} catch (Exception e) {
						e.printStackTrace();
					}

					//Empty the list before the loop begins processing the next statement
					processedIntent.clear();


					try {
						pchn2.insertBeforeNoRedirect(sitnCall, curUnit);
					} catch (Exception e) {
						e.printStackTrace();
					}

					body.validate();
				}


				else
				{
					processedIntent.clear();

				}




			} ///<=== if statements for parsing out from locals being matched to unexpandaded locals inside captured invoke statements

			else if(compStmt.contains(IntCompStr))
			{

				StringConstant sootStr= StringConstant.v(compStmt);

				Stmt sitnCall = null;
				try {
					sitnCall = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(printrawSt.makeRef(), sootStr));
				} catch (Exception e) {
					e.printStackTrace();
				}

				//Empty the list before the loop begins processing the next statement
				processedIntent.clear();


				try {
					pchn2.insertBeforeNoRedirect(sitnCall, curUnit);
				} catch (Exception e) {
					e.printStackTrace();
				}

				body.validate();


			}

			else {}
			


			/*
			currentUnit.apply(new AbstractStmtSwitch() {



				@SuppressWarnings("null")
				public void caseInvokeStmt(InvokeStmt stmt) {




					Local tmpRef = null;
					Local tmpString = null;
					try {
						tmpRef = addTmpRef(body);
						tmpString = addTmpString(body);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}


					//NOTE:insertBEFORE must be chosen, or the emulator will generate java.lang.verify errors...


					// insert "tmpRef = java.lang.System.out;"
					pchn.insertBefore(Jimple.v().newAssignStmt(
							tmpRef, Jimple.v().newStaticFieldRef(
									Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), currentUnit);

					// insert "tmpLong = 'HELLO';"
					pchn.insertBefore(Jimple.v().newAssignStmt(tmpString,
							StringConstant.v((finalLogCatOutputString))), currentUnit);

					// insert "tmpRef.println(tmpString);"
					SootMethod toCall = Scene.v().getSootClass("java.io.PrintStream").getMethod("void println(java.lang.String)");
					pchn.insertBefore(Jimple.v().newInvokeStmt(
							Jimple.v().newVirtualInvokeExpr(tmpRef, toCall.makeRef(), tmpString)), currentUnit);





					//check that we did not mess up the Jimple
					body.validate();

				} //<===caseInvokeStatement

			}); //<=====abstract switch statement for embedding output
			
			*/






		} //<=== filter for re-iterating through body with detected Intent















	}

/*

	private static Local addTmpRef(Body body)
	{
		Local tmpRef = Jimple.v().newLocal("tmpRef", RefType.v("java.io.PrintStream"));
		body.getLocals().add(tmpRef);
		return tmpRef;
	}

	private static Local addTmpString(Body body)
	{
		Local tmpString = Jimple.v().newLocal("tmpString", RefType.v("java.lang.String"));
		body.getLocals().add(tmpString);
		return tmpString;
	}

	*/
}




				/*


				if(matchStmt.contains("$r"))
				{



					ArrayList<Integer> idxsOfInts = new ArrayList<Integer>();

					//Grab all the locals out of the body, for matching process implemented against r values in function below.

					Chain<Local> comparLocs = body.getLocals();
					Iterator<Local> LocalIterator = comparLocs.iterator();


					//Iterate through comparison string, find all integers and capture them
					for (int i = 0; i < matchStmt.length(); i++)
					{

						if (Character.isDigit(matchStmt.charAt(i))) {

							//Finds the indexes of integers contained in the invoke statement, and captures them for processing
							idxsOfInts.add(Integer.parseInt((matchStmt.substring(i, i+1)))); // found a digit



						}
					}

					//Creates an iterator to process the captured integers
					Iterator<Integer> idxIntIT = idxsOfInts.iterator();

					while(LocalIterator.hasNext())
					{
						Local localComp = LocalIterator.next();

						//Gets the name of the Local for processing comparisons
						String nameOfCompLocal = localComp.getName().toString();

						while(idxIntIT.hasNext())

						{
							Integer compInt= idxIntIT.next();

							//Create a comparison string with an r in front the int, to make textual matching possible within the locals name
							String match = "r" + compInt.toString();




							if(nameOfCompLocal.contains(match.toString()))
							{
								//APK output
								String variablePass =  "The unexpanded variable in the statement: " + localComp.getName() + ", is of type: " + localComp.getType().toString() + "\n";
								expVars.add(variablePass);

							}



						} //<===end of index iterator

						//resets iterator for next comparison of integers against Locals being iterated
						idxIntIT = idxsOfInts.iterator();

					} //<===end of Local iterator


				} //<=== end of r filter

				*/


// processedIntent.add(expVars);




//This filter searches for unexpanded variables in the above statement/argument

				/*

				if(matchStmt.contains("$r"))
				{

					ArrayList<Integer> idxsOfInts = new ArrayList<Integer>();

					//Grab all the locals out of the body, for matching process implemented against r values in function below.

					Chain<Local> comparLocs = body.getLocals();
					Iterator<Local> LocalIterator = comparLocs.iterator();


					//Iterate through comparison string, find all integers and capture them
					for (int i = 0; i < matchStmt.length(); i++)
					{

						if (Character.isDigit(matchStmt.charAt(i))) {

							//Finds the indexes of integers contained in the invoke statement, and captures them for processing
							idxsOfInts.add(Integer.parseInt((matchStmt.substring(i, i+1)))); // found a digit



						}
					}

					//Creates an iterator to process the captured integers
					Iterator<Integer> idxIntIT = idxsOfInts.iterator();





					while(LocalIterator.hasNext())
					{
						Local localComp = LocalIterator.next();

						//Gets the name of the Local for processing comparisons
						String nameOfCompLocal = localComp.getName().toString();

						while(idxIntIT.hasNext())

						{
							Integer compInt= idxIntIT.next();

							//Create a comparison string with an r in front of the captured integer, to make textual matching possible within the locals name
							String match = "r" + compInt.toString();




							if(nameOfCompLocal.contains(match.toString()))
							{

								String variablePass =  "The unexpanded variable in the statement: " + localComp.getName() + ", is of type: " + localComp.getType().toString() + "\n";
								expVars.add(variablePass);

							}



						}

						//resets iterator for next comparison of integers against Locals being iterated
						idxIntIT = idxsOfInts.iterator();




					} //<====End of local iterator


				} //<=== end of r (unexpanded variable) filter

				*/



//processedIntent.add(expVars);