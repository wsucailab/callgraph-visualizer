package Instrument;

/**
 * Created by Sabatu on 4/5/2017.
 */

import org.jgraph.JGraph;
import org.jgraph.graph.*;
import org.jgrapht.ListenableGraph;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.ListenableDirectedGraph;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.*;
import java.util.List;

import static fj.function.Strings.contains;
import static java.awt.Color.blue;

public class Flipper extends JApplet {

    private static final Color     DEFAULT_BG_COLOR = Color.decode( "#FAFBFF" );
    private static final Dimension DEFAULT_SIZE = new Dimension( 1060, 640 );
    public static ListenableGraph g;


    private JGraphModelAdapter m_jgAdapter;
    private JGraph jgraph;

    private FlipTask flipTask;

    public String beta = new String();
    long charlie = 0;

    //beginning of class direction
    public Flipper() {

        // create a JGraphT graph
        g = new ListenableDirectedGraph( DefaultEdge.class );

        // create a visualization using JGraph, via an adapter
        m_jgAdapter = new JGraphModelAdapter( g );

        jgraph = new JGraph( m_jgAdapter );




        adjustDisplaySettings( jgraph );

        getContentPane(  ).add( jgraph );

        resize( DEFAULT_SIZE );

        System.out.println("This ran");

        jgraph.setEnabled(true);



        setVisible(true);



    }

    public void updateGraph(String intentType, int x, int y) {

        Object a = createVertex(intentType, 50, 30, 20, 20, null, true);

        //g.addVertex(intentType);

        g.addVertex(a);

        positionVertexAt(a, x, y);

        setColors();
       // setSize();

        jgraph.repaint();

    }

    public void setColors() {
        GraphLayoutCache cache = jgraph.getGraphLayoutCache();
        for (Object item : jgraph.getRoots()) {
            GraphCell cell = (GraphCell) item;
            CellView view = cache.getMapping(cell, true);
            AttributeMap map = view.getAttributes();
            map.applyValue(GraphConstants.BACKGROUND, Color.BLUE);
        }
        cache.reload();
        jgraph.repaint();
    }

    public void setSize() {

        GraphLayoutCache cache = jgraph.getGraphLayoutCache();

        CellView[] cells = cache.getCellViews();

        for (CellView cell : cells) {

            if(cell instanceof VertexView)
            {
                Rectangle2D a = new Rectangle2D() {
                    @Override
                    public void setRect(double x, double y, double w, double h) {

                    }

                    @Override
                    public int outcode(double x, double y) {
                        return 0;
                    }

                    @Override
                    public Rectangle2D createIntersection(Rectangle2D r) {
                        return null;
                    }

                    @Override
                    public Rectangle2D createUnion(Rectangle2D r) {
                        return null;
                    }

                    @Override
                    public double getX() {
                        return 0;
                    }

                    @Override
                    public double getY() {
                        return 0;
                    }

                    @Override
                    public double getWidth() {
                        return 0;
                    }

                    @Override
                    public double getHeight() {
                        return 0;
                    }

                    @Override
                    public boolean isEmpty() {
                        return false;
                    }
                };

                a.setRect(200,200,200,200);



                ((VertexView) cell).setBounds(a);

                AttributeMap b = new AttributeMap();

                b.createRect(a);

                ((VertexView) cell).setAttributes(b);
            }



        }
        cache.reload();
        jgraph.repaint();
    }


    private void adjustDisplaySettings( JGraph jg ) {
        jg.setPreferredSize( DEFAULT_SIZE );

        Color  c        = DEFAULT_BG_COLOR;
        String colorStr = null;

        try {
            colorStr = getParameter( "bgcolor" );
        }
        catch( Exception e ) {}

        if( colorStr != null ) {
            c = Color.decode( colorStr );
        }

        jg.setBackground( c );
    }




    private void positionVertexAt( Object vertex, int x, int y ) {

        DefaultGraphCell cell = m_jgAdapter.getVertexCell( vertex );

        Map attr = cell.getAttributes(  );

        AttributeMap VertexAttributes = new AttributeMap();

        Rectangle2D b =  GraphConstants.getBounds( attr );



        GraphConstants.setBounds( attr, new Rectangle( x, y, (int) b.getWidth(), (int) b.getHeight() ) );

        Map cellAttr = new HashMap(  );
        cellAttr.put( cell, attr );
        m_jgAdapter.edit( cellAttr, null, null, null );
    }



    public static DefaultGraphCell createVertex (String name, double x, double y, double w, double h,
                                                 Color bg, boolean raised)
    {
        DefaultGraphCell cell = new DefaultGraphCell (name);

        GraphConstants.setBounds (cell.getAttributes (), new Rectangle2D.Double (x, y, w, h));

        if (bg != null)
        {
            GraphConstants.setGradientColor (cell.getAttributes (), Color.orange);
            GraphConstants.setOpaque (cell.getAttributes (), true);
        }

        if (raised)
            GraphConstants.setBorder (cell.getAttributes (), BorderFactory.createRaisedBevelBorder ());
        else
            GraphConstants.setBorderColor (cell.getAttributes (), Color.black);

        DefaultPort port = new DefaultPort ();
        cell.add (port);

        return cell;
    }



















    //This.. might be where we can enter?
    public class FlipPair {

        private long total;
        private String heads = "Hello";

        FlipPair(String heads, long total)
        {
            this.heads = heads;
            this.total = total;
        }
    }


    /*
    This method contains publish():

    Sends data chunks to the process method. This method is to be used from inside the doInBackground method to deliver
    intermediate results for processing on the Event Dispatch Thread inside the process method.

    Because the process method is invoked asynchronously on the Event Dispatch Thread multiple invocations
    to the publish method might occur before the process method is executed.

    For performance purposes all these invocations are coalesced into one invocation with concatenated arguments.

    ALSO:

    Class SwingWorker<T,V>

    java.lang.Object
        javax.swing.SwingWorker<T,V>

    Type Parameters:
        T - the result type returned by this SwingWorker's doInBackground and get methods
        V - the type used for carrying out intermediate results by this SwingWorker's publish and process methods




     */

    private class FlipTask extends SwingWorker<Void, FlipPair> {


        @Override
        protected Void doInBackground() {

            String heads = "Hello";
            long total = 0;

            Random random = new Random();

            //As long as the task isnt prematurely cancelled....
            while (!isCancelled()) {

                total++;

                /*
                The nextBoolean() method is used to get the next pseudorandom,
                 uniformly distributed boolean value from this random number generator's sequence.
                 */

                if (random.nextBoolean())
                {
                    //heads++;

                    heads = beta;
                }

                publish(new FlipPair(heads, total));
            }
            return null;
        }

        /*
             Receives data chunks from the publish method asynchronously on the Event Dispatch Thread.
             Please refer to the publish method for more details.

             Soo.. okay. this thread is asynchronously then updating the textfields within the app.
         */
        @Override
        protected void process(List<FlipPair> pairs) {


            /*

            FlipPair pair = pairs.get(pairs.size() - 1);
            headsText.setText(String.format("%s", pair.heads));
            totalText.setText(String.format("%d", pair.total));
            //devText.setText(String.format("%.10g",
              //      ((String) pair.heads)/((double) pair.total) - 0.5));

              */



        }
    }



    //Starts a swing thread, passes it a new instance of Flipper
    public void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                new Flipper();
            }
        });


    } //<===main method

} //<===public class flipper