package Instrument;

/**
 * Created by Sabatu on 4/4/2017.
 */
import java.awt.*;

import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import org.jgraph.JGraph;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

import org.jgrapht.ListenableGraph;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.ListenableDirectedGraph;
import org.jgrapht.graph.DefaultEdge;


/**
 * A demo applet that shows how to use JGraph to visualize JGraphT graphs.
 *
 * @author Barak Naveh
 *
 * @since Aug 3, 2003
 */
public class ICCgraphGen extends JApplet implements Runnable {

    private Thread t;
    private int pause;

    private static final Color     DEFAULT_BG_COLOR = Color.decode( "#FAFBFF" );
    private static final Dimension DEFAULT_SIZE = new Dimension( 1060, 640 );


    private JGraphModelAdapter m_jgAdapter;



    public void main() {

        SwingUtilities.invokeLater(new Runnable() { public void run() { new ICCgraphGen(); init();}  });

    }

    @Override
    public void run() {

        init();
    }

    /*
    public ICCgraphGen(pause) {

        this.pause = pause;
        t = new Thread(this);
        t.start();
    }

    public void run() {
        while (true) {

            init();

            //repaint();
            try {
                t.sleep(pause);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

    }

    */


    /**
     * @see java.applet.Applet#init().
     */
    public void init(  ) {

        // create a JGraphT graph
        ListenableGraph g = new ListenableDirectedGraph( DefaultEdge.class );

        // create a visualization using JGraph, via an adapter
        m_jgAdapter = new JGraphModelAdapter( g );

        JGraph jgraph = new JGraph( m_jgAdapter );

        adjustDisplaySettings( jgraph );
        getContentPane(  ).add( jgraph );
        resize( DEFAULT_SIZE );

        //GraphConstants.setResize();




        // add some sample data (graph manipulated via JGraphT)

        //Einen nodes
        g.addVertex( "Whatever" );
       // g.addVertex( "v2");
       // g.addVertex( "v3" );
       // g.addVertex( "v4" );

        //Einen edges
       // g.addEdge( "Whatever", "v2" );
       // g.addEdge( "v2", "v4", "a label maybe?" );
       // g.addEdge( "v3", "Whatever" );
      //  g.addEdge( "v4", "v3" );

        // position vertices nicely within JGraph component
        positionVertexAt( "Whatever", 130, 40 );
       // positionVertexAt( "v2", 60, 200 );
       // positionVertexAt( "v3", 310, 230 );
       // positionVertexAt( "v4", 380, 70 );

        System.out.println("This ran");

        jgraph.setEnabled(true);

        setVisible(true);

        // that's all there is to it!...

    }


    public ListenableGraph createGraph(){

        init();

        // create a JGraphT graph
        ListenableGraph g = new ListenableDirectedGraph( DefaultEdge.class );

        // create a visualization using JGraph, via an adapter
        m_jgAdapter = new JGraphModelAdapter( g );

        JGraph jgraph = new JGraph( m_jgAdapter );

        adjustDisplaySettings( jgraph );
        getContentPane(  ).add( jgraph );
        resize( DEFAULT_SIZE );

        //GraphConstants.setResize();




        // add some sample data (graph manipulated via JGraphT)

        //Einen nodes
        g.addVertex( "Whatever" );
        g.addVertex( "v2");
        g.addVertex( "v3" );
        g.addVertex( "v4" );

        //Einen edges
        g.addEdge( "Whatever", "v2" );
        g.addEdge( "v2", "v4", "a label maybe?" );
        g.addEdge( "v3", "Whatever" );
        g.addEdge( "v4", "v3" );

        // position vertices nicely within JGraph component
        positionVertexAt( "Whatever", 130, 40 );
        positionVertexAt( "v2", 60, 200 );
        positionVertexAt( "v3", 310, 230 );
        positionVertexAt( "v4", 380, 70 );

        // that's all there is to it!...



        return g;
    }

    public ListenableGraph updateGraph(ListenableGraph g) {

       g.addVertex("new node added");

        return g;

    }


    private void adjustDisplaySettings( JGraph jg ) {
        jg.setPreferredSize( DEFAULT_SIZE );

        Color  c        = DEFAULT_BG_COLOR;
        String colorStr = null;

        try {
            colorStr = getParameter( "bgcolor" );
        }
        catch( Exception e ) {}

        if( colorStr != null ) {
            c = Color.decode( colorStr );
        }

        jg.setBackground( c );
    }




    private void positionVertexAt( Object vertex, int x, int y ) {
        DefaultGraphCell cell = m_jgAdapter.getVertexCell( vertex );
        Map attr = cell.getAttributes(  );

        Rectangle2D b =  GraphConstants.getBounds( attr );


        GraphConstants.setBounds( attr, new Rectangle( x, y, (int) b.getWidth(), (int) b.getHeight() ) );

        Map cellAttr = new HashMap(  );
        cellAttr.put( cell, attr );
        m_jgAdapter.edit( cellAttr, null, null, null );
    }
}