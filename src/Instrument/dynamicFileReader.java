package Instrument;

/**
 * Created by Sabatu on 4/4/2017.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Timer;
import java.util.TimerTask;


public class dynamicFileReader {

    /**
     * @param args
     */
    public static void main(String[] args) {

        File file = new File("/home/semika/apache-tomcat-7.0.25/logs/catalina.out");

        try {

            RandomAccessFile r = new RandomAccessFile(file, "r");

            //First time read
            String str = null;

            while((str = r.readLine()) != null) {

                System.out.println(str);

            }

            r.seek(r.getFilePointer());

            startTimer(r);
        }
        catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        catch (IOException e) {e.printStackTrace();
        }
    }

    private static void startTimer(final RandomAccessFile r) {

        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                String str = null;
                try {
                    while((str = r.readLine()) != null) {
                        System.out.println(str);
                    }

                    r.seek(r.getFilePointer());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1000);
    }
}

