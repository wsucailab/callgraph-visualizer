package Instrument;

import vtk.*;

/**
 * Created by Sabatu on 4/12/2017.
 */
public class vtkGraph3 {


    static {
        try {
            if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
                for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
                    if (!lib.IsLoaded())
                        System.out.println(lib.GetLibraryName() + " not loaded");
                }
                System.out.println("Make sure the search path is correct: ");
                System.out.println(System.getProperty("java.library.path"));
            }
            vtkNativeLibrary.DisableOutputWindow(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * primary test driver, creates an instance of this class
     * and then runs the example function.

     */
    public static void main(String[] args) {

        //vtkGraph me = new vtkGraph();
        //me.doit();

        vtkGraph3 me3 = new vtkGraph3();

        me3.routine();









    }

    public void routine(){

       // vtkDirectedGraph ad = new vtkDirectedGraph();

        vtkMutableDirectedGraph ad = new vtkMutableDirectedGraph();



        vtkIntArray vertId = new vtkIntArray();

        vtkStringArray labels = new vtkStringArray();

        labels.SetName("Label");

        vertId.SetName("id");

        ad.GetVertexData().AddArray(vertId);

        int v;

        int[] vector = new int[10];

        vtkPoints points = new vtkPoints();


        int x = 0;
        int y = 0;
        int z = 0;

        for (v = 0; v < 10; v++)
        {
            vector[v] =  ad.AddVertex();

            vertId.InsertNextValue(v);

            labels.InsertValue(v, "label");

            points.InsertNextPoint(x, y, z);



            //No noticeable difference..
            x += 20;


           //Distance from Camera (looks like scaling perhaps)
            y+=20;

            //Distance from each other
            z+=00;        }



        ad.SetPoints(points);



        ad.GetVertexData().AddArray(labels);

        //Dynamically Generate Edges
        for (int e = 0; e < 10; ++e)
        {
            //a.AddGraphEdge(e, (e+1)%10);

            ad.AddGraphEdge(e, (e+1)%10);
        }


        //vtkFast2DLayoutStrategy strategy =new vtkFast2DLayoutStrategy();

        vtkSimple2DLayoutStrategy strategy =new vtkSimple2DLayoutStrategy();

        //strategy.SetWeightEdges(true);

        //#strategy = vtkSimple2DLayoutStrategy()
        //#strategy = vtkCosmicTreeLayoutStrategy()
        //#strategy = vtkForceDirectedLayoutStrategy()
        //#strategy = vtkTreeLayoutStrategy()
        //#set the strategy on the layout

        vtkGraphLayout layout = new vtkGraphLayout();
        layout.SetLayoutStrategy(strategy);

        layout.SetInputData(ad);

        //    #create the renderer to help in sizing glyphs for the vertices




        vtkRenderer ren = new vtkRenderer();

              //#Pipeline for displaying vertices -glyph -> mapper -> actor -> display
        //#mark each vertex with a circle glyph

        ren.InteractiveOn();
        //ren.MakeLight();

       // ren.WorldToView();

        vtkGraphToGlyphs vertex_glyphs =new vtkGraphToGlyphs();

        vertex_glyphs.SetInputConnection(layout.GetOutputPort());
        vertex_glyphs.SetGlyphType(8);
        vertex_glyphs.FilledOn();

        //vertex_glyphs.SetScaling(true);

        vertex_glyphs.SetRenderer(ren);

        // ren.DeviceRender();

        //    #create a mapper for vertex display
  //vtkGlyphSource2D
       // vtkPolyDataMapper vertex_mapper = new vtkPolyDataMapper();

        vtkPolyDataMapper vertex_mapper = new vtkPolyDataMapper();
        vertex_mapper.SetInputConnection(vertex_glyphs.GetOutputPort());
        vertex_mapper.SetScalarRange(0, 100);
        vertex_mapper.SetScalarModeToUsePointFieldData();
        vertex_mapper.SelectColorArray("vertex id");

        vertex_mapper.Update();

        //vtkActor2D vertex_actor = new vtkActor2D();

        //#create the actor for displaying vertices
       vtkActor vertex_actor = new vtkActor();

       vertex_actor.SetMapper(vertex_mapper);

        //vertex_actor.SetOrientation(0,45,0);

        //vertex_actor2.SetOrientation(90,0,0);

        //vertex_actor.SetScale(0,0,1);

        vertex_actor.GetProperty().SetColor(0,100,100);

        vertex_actor.GetProperty().SetLighting(true);

        //Pipeline for displaying edges of the graph - layout -> lines -> mapper-> actor -> display
        //#NOTE:
        //  If no edge layout is performed, all edges will be rendered as
        //#line segments between vertices in the graph.

         vtkArcParallelEdgeStrategy edge_strategy = new vtkArcParallelEdgeStrategy();

          vtkEdgeLayout edge_layout = new vtkEdgeLayout();
         edge_layout.SetLayoutStrategy(edge_strategy);
          edge_layout.SetInputConnection(layout.GetOutputPort());

         vtkGraphToPolyData edge_geom = new vtkGraphToPolyData();
         edge_geom.SetInputConnection(edge_layout.GetOutputPort());

        //#create a mapper for edge display

          vtkPolyDataMapper edge_mapper = new vtkPolyDataMapper();


          edge_mapper.SetInputConnection(edge_geom.GetOutputPort());

        //#create the actor for displaying the edges

          vtkActor edge_actor = new vtkActor();
          edge_actor.SetMapper(edge_mapper);
        edge_actor.GetProperty().SetColor(0., 0., 0.);
         edge_actor.GetProperty().SetOpacity(0.25);
//

      //  vtkRenderer, mapper
        //edge_actor.Render(ren, edge_mapper);


        vtkRenderer a = new vtkRenderer();
        vtkRenderWindow b = new vtkRenderWindow();

        a.SetBackground(100,100,100);


        b.AddRenderer(a);

        vtkRenderWindowInteractor c = new vtkRenderWindowInteractor();

        c.SetRenderWindow(b);


        a.AddActor(edge_actor);

        a.AddActor(vertex_actor);

        a.AutomaticLightCreationOn();

        b.Render();



        //vtkInteractorStyleTrackballCamera d = new vtkInteractorStyleTrackballCamera();

        //vtkInteractorStyleMultiTouchCamera

       // vtkInteractorStyleJoystickCamera

        vtkInteractorStyleJoystickCamera d = new vtkInteractorStyleJoystickCamera();

        c.SetInteractorStyle(d);

        c.Start();







    }

}
