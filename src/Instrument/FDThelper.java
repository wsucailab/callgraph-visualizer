package Instrument;

import android.content.Intent;

import java.util.Iterator;

/**
 * Created by Sabatu on 3/17/2017.
 */
public class FDThelper {

    public static void intentDump(Intent foundYou)
    {
        System.out.println("Intent found. Printing relevant information:");


        System.out.println("Here's its toString method: " + foundYou.toString());

        System.out.println("Here's the action: " + foundYou.getAction());

        System.out.println("Here's the type: " + foundYou.getType());

        if (!foundYou.getCategories().isEmpty()) {
            Iterator<String> cats = foundYou.getCategories().iterator();
            int i = 1;
            while (cats.hasNext()) {
                System.out.println("Here's category number " + i + ": " + cats.next());
            }
        }

    }
}
