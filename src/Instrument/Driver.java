package Instrument;



import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;



import soot.*;
import soot.jimple.AbstractStmtSwitch;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Edge;
import soot.options.*;



    
public class Driver extends SceneTransformer {
          
         
          
          
public static void main(String[] args) {
    
          /* check the arguments */
           if (args.length == 0) {
            System.err.println("Usage: java Driver [options] classname");
            System.exit(0);
          }

    //prefer Android APK files// -src-prec apk
    Options.v().set_src_prec(Options.src_prec_apk);
   
    System.err.println("Main in Driver ran");

    //output as APK, too//-f J
    Options.v().set_output_format(Options.output_format_dex);

    // resolve the PrintStream and System soot-classes
    Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
    Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
   

   
   
    //Okay, THIS is calling the methods internal transform. The run method is doing absolutely fucking nothing.
    PackManager.v().getPack("wjtp").add(new Transform("wjtp.mt", new Driver()));

   

   
   
          /* Give control to Soot to process all options,
           * InvokeStaticInstrumenter.internalTransform will get called.
           */
          soot.Main.main(args);
      }
          
          
          
           //So this isnt meant to run this methods internal transform. It works with internal transform below, to call the internal transform of of other methods.
           //The for loop isn't getting called for some reason, though..
            @SuppressWarnings("rawtypes")
            public void run() {
                internalTransform("", new HashMap());
               
                System.err.println("Run in Driver ran");

            }
 
            @Override
            protected void internalTransform(String phaseName, @SuppressWarnings("rawtypes") Map options) {   
               
                String newLine = System.getProperty("line.separator");
               
                /*Log Implementation
                
                Scene.v().addBasicClass("android.util.Log",SootClass.SIGNATURES);
                
                SootMethod sm=Scene.v().getMethod("<android.util.Log: int i(java.lang.String,java.lang.String)>");
           
                Value logType = StringConstant.v("INFO");
                
                Value logMessage = StringConstant.v("Inserted Log after show()");
                
                StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm.makeRef(), logType, logMessage);
                
                Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
                
                */
                
                
                
                Iterator<SootClass> clsIt = Scene.v().getClasses().snapshotIterator();
               
                while (clsIt.hasNext()) {
                    SootClass sClass = clsIt.next();
                   
                    if ( sClass.isPhantom() ) {
                        // skip phantom classes
                        continue;
                    }
                    
                    if (sClass.isInterface()) continue;
                    
                    if (sClass.isInnerClass()) continue;
                    
                    if ( !sClass.isApplicationClass() ) {
                        // skip library classes
                        continue;
                    }      
             
                   
                    /* traverse all methods of the class */
                    Iterator<SootMethod> meIt = sClass.getMethods().iterator();
                   
                    while (meIt.hasNext()) {
                       
                        SootMethod sMethod = meIt.next();
                       
                        //System.out.println("\n method visited - " + sMethod );
                       
                        if ( !sMethod.isConcrete() ) {
                           
                            // skip abstract methods and phantom methods, and native methods as well
                            continue;
                        }
                       
                        if ( sMethod.toString().indexOf(": java.lang.Class class$") != -1 ) {
                            // don't handle reflections now either
                            continue;
                        }
                       
                        
                        
                        
                        
                        // cannot instrument method event for a method without active body
                        //if ( !sMethod.hasActiveBody() ) {
                        //    continue;
                       // }
             
                        //Body body = sMethod.getActiveBody();
                        
                        
                       
                        final Body body = sMethod.retrieveActiveBody();
                        
                        
                        
                        

             
                       
                        /* the ID of a method to be used for identifying and indexing a method in the event maps of EAS */
                        //String meId = sClass.getName() +    "::" + sMethod.getName();
                       // String meId = sMethod.getSignature();
                       
                        final PatchingChain<Unit> pchn = body.getUnits();
                        
                        //CFG cfg = ProgramFlowGraph.inst().getCFG(sMethod);
                       
                       
                       Iterator<Unit> uiter = pchn.snapshotIterator();
                       

                       
                        while (uiter.hasNext()) {
                        	
                        	//This is implementation code for printing something out to the logcat console, given a condition
                        	
                        	//begin TUTORIAL code
                        	
                        	final Unit u = uiter.next();
                        	
                        	
                        	
                        	
        					u.apply(new AbstractStmtSwitch() {
        						
        						
        						
        						public void caseInvokeStmt(InvokeStmt stmt) {
        							
        							
        							InvokeExpr invokeExpr = stmt.getInvokeExpr();
        							
        							
        							
        							if(invokeExpr.getMethod().getName().equals("onStart")) {

        								Local tmpRef = null;
										Local tmpString = null;
										try {
											tmpRef = addTmpRef(body);
											tmpString = addTmpString(body);
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
        								
        								  // insert "tmpRef = java.lang.System.out;" 
        						        pchn.insertBefore(Jimple.v().newAssignStmt( 
        						                      tmpRef, Jimple.v().newStaticFieldRef( 
        						                      Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), u);

        						        // insert "tmpLong = 'HELLO';" 
        						        pchn.insertBefore(Jimple.v().newAssignStmt(tmpString, 
        						                      StringConstant.v("HELLO")), u);
        						        
        						        // insert "tmpRef.println(tmpString);" 
        						        SootMethod toCall = Scene.v().getSootClass("java.io.PrintStream").getMethod("void println(java.lang.String)");                    
        						        pchn.insertBefore(Jimple.v().newInvokeStmt(
        						                      Jimple.v().newVirtualInvokeExpr(tmpRef, toCall.makeRef(), tmpString)), u);
        						        
        						        //check that we did not mess up the Jimple
        						        body.validate();
        							}
        						}
        						
        					});
        					
        					
        					
        					//end TUTORIAL code
        					
        					
        					
        					
        					//begin INSTRUMENTATION code
        					
        					
                        	
                             Stmt s = null;
							String comparison = null;
							
								s = (Stmt)u;   
								
								final Unit a = (Unit) s.clone();
								
								 comparison = s.toString();
							
								// TODO Auto-generated catch block
							
							
                             
                             
                            
                             //S is the statement inside the returned method. So, if this individual statement is an API that can send an intent
                         
							//BLOCKED FILTER FINDCOMPONENTTYPE
							
							//  if (FindComponentType.is_IntentSendingAPI(comparison)) {
							
							
							
							
							
                               
                                //Then create a new list                   
                                //List<Object> itnProbes = new ArrayList<Object>();
                               
                                //Create another list. This will store any captured intents found in the statement, they've been iterated through.
                                //List itnArgs = new ArrayList();
                               
                                //Get the invoked expression.. (returns a statement, this is basically turning it into a static reference)        
                            	
                            	if(!s.containsInvokeExpr())
                            	{
                            		continue;
                            	}
                            	
                            	
                            	
                            	InvokeExpr inv = null;
								try {
									inv = s.getInvokeExpr();
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}   
                            	
                            	
                            
            					
                               
                                //This loop is specifically looking for Intents...
								
								
								
                                for (int idx = 0; idx < inv.getArgCount(); idx++) {
                                	
                                	
                                   
                                    Value curarg = null;
									try {
										curarg = inv.getArg(idx);
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									
									String curargSTR = curarg.toString();
                                   
                                    if (curarg.getType().equals(Scene.v().getRefType("android.content.Intent"))) {
                                    	
                                    	
                                    	
                    					u.apply(new AbstractStmtSwitch() {
                    						
                    						
                    						
                    						@SuppressWarnings("null")
											public void caseInvokeStmt(InvokeStmt stmt) {
                    							
                    							
                    							

                    								Local tmpRef = null;
													Local tmpString = null;
													try {
														tmpRef = addTmpRef(body);
														tmpString = addTmpString(body);
													} catch (Exception e1) {
														// TODO Auto-generated catch block
														e1.printStackTrace();
													}
                    								
                    								CallGraph programCG = Scene.v().getCallGraph();
													
													Iterator<Edge> edgesIn = programCG.edgesInto(sMethod);
													Iterator<Edge> edgesOutOf = programCG.edgesOutOf(sMethod);
													
													
											      
													
													
                    									 
                    									  
                    									  //NOTE:insertBEFORE must be chosen, or the emulator will generate java.lang.verify errors...
                    									 
                    									 String output = "An intent was generated. It came from method: " + sMethod.toString();
                    									
                    									
                    									 /*
                    									  
                    									
                    									
                    										
                    											output = "In intent-based method: " + sMethod.toString() + " has edges going in: "+ edgesIn.hasNext() + " and edges going out: " + edgesOutOf.hasNext() 
                    													+ " has found edge " + something.toString();
                    													
                    													*/
                    									 
                    									 
                    									   List<Type> params = sMethod.getParameterTypes();
    		     					                       Iterator<Type> paramIT = params.iterator();
    		     					                       int parameter_count = 0;
    		     					                       while(paramIT.hasNext())
    		     					                       {
    		     					                    	   Type para = paramIT.next();
    		     					                    	   //System.out.println("Parameter: " + para.toString());
    		     					                    	   //output += ".. with parameter #: " + parameter_count + " : " + para.toString();
    		     					                    	   
    		     					                    	   
    		     					                    	   if(para.toString() == "android.content.Intent")
    		     					                    	   {
    		     					                    		  //output += "The escaped name is: " + para.getEscapedName() + " ";
    		     					                    		// output += "The class is: " + para.getClass().toString() + " ";  
    		     					                    		//output += "The number is: " + para.getNumber() + " ";  
    		     					                    		
    		     					                    		
    		     					                    	   }
    		     					                       }
    		     					                       
    		     					                       parameter_count = 0;
    		     					                       
    		     					                       //output += "....AND THE CLASS IS: " + sMethod.getDeclaringClass();
    		     					                       //output += "...and the stupid class is: " + sMethod.getClass().toString();
    		     					                       //output += "...and the declaration is: " + sMethod.getDeclaration();
                    									
                    									
                    									 SootClass testDec = Scene.v().getSootClass(sMethod.getDeclaration());
                    									 
                    									 output += " and the declaring class was: " + testDec.toString();
                    									 output += " FindComponentType says: " + FindComponentType.getComponentType(testDec);
                    									 
                    									 // String output = "This is a sending intentAPI. sMethod intent found in'" + sMethod.toString() + " curarg intent found in curarg: " + curargSTR +
                    										//
                    									  
														// insert "tmpRef = java.lang.System.out;" 
														pchn.insertBefore(Jimple.v().newAssignStmt( 
														              tmpRef, Jimple.v().newStaticFieldRef( 
														              Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), u);

														// insert "tmpLong = 'HELLO';" 
														pchn.insertBefore(Jimple.v().newAssignStmt(tmpString, 
														              StringConstant.v((output))), u);
														
														// insert "tmpRef.println(tmpString);" 
														SootMethod toCall = Scene.v().getSootClass("java.io.PrintStream").getMethod("void println(java.lang.String)");                    
														pchn.insertBefore(Jimple.v().newInvokeStmt(
														              Jimple.v().newVirtualInvokeExpr(tmpRef, toCall.makeRef(), tmpString)), u);
														
												
                    						        
                    						        
                    						        
                    						        //check that we did not mess up the Jimple
                    						        body.validate();
                    							
                    					}
                    						
                    					});
                                    	
                                    	
                                    	
                                    	
                                    	
                                    	
                                    	
                                    	
                                    	
                                       
                                        System.out.println("So, this should be what curarg.getType() looks like: " + curarg.getType().toString() + "\n");
                                        System.out.println("Coming from this statement: " + s.toString() + "\n");
                                        System.out.println("This is curarg: " + curarg.toString() + "\n");
                                        System.out.println("and this methods signature: " + sMethod.getSignature() + "\n");
                                        System.out.println("and this method: " + sMethod.toString() + "\n");
                                        System.out.println(newLine);
                                  
                                     //   try {
											//android.util.Log.e("[Jenkins]This should be an intent", curarg.toString());
										//	android.util.Log.e("[Jenkins]This should be the statement it comes from", s.toString());
										//	android.util.Log.e("[Jenkins]From this chain", pchn.toString());
										//	android.util.Log.e("[Jenkins]this method", sMethod.toString());
										//	android.util.Log.e("[Jenkins]..and this class", sClass.toString());
										//} catch (Exception e) {
										//	// TODO Auto-generated catch block
									//		e.printStackTrace();
									//	}
                                       
                                     //   break;
                                       
                                       
                                       
                                       
                                       
                                        //print out all intents
                                       
                                       
                                        //itnArgs.add(curarg);
                                       
                                        //itnArgs.add(utils.makeBoxedValue(sMethod, curarg, itnProbes));
                                       
                                       
                                    
                                    
                                    
                                } //< == for loop for iterating  through arguments
                       
                 
                            
                            
                            
                            
                            
                           
                   					
                               
                                }
                               
                               
                      
                               } //<===statements
                               } //<===soot methods
                           } //<==sootclasses
                    } //< == internalTransform
                
                
                
                
                
                
                
                
                
                
            
                           
                           
                   
               


               
                       
                       
                       
                       
                             //<==while uiter has next 
                       
                   
               
                         //<==while method iterate has next
                   
                //<==while class iterate has next
         //<===internal transform
            
            
            private static Local addTmpRef(Body body)
            {
                Local tmpRef = Jimple.v().newLocal("tmpRef", RefType.v("java.io.PrintStream"));
                body.getLocals().add(tmpRef);
                return tmpRef;
            }
            
            private static Local addTmpString(Body body)
            {
                Local tmpString = Jimple.v().newLocal("tmpString", RefType.v("java.lang.String")); 
                body.getLocals().add(tmpString);
                return tmpString;
            }
}