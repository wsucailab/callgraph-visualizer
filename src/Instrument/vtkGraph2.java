package Instrument;

import vtk.*;

/**
 * Created by Sabatu on 4/12/2017.
 */
public class vtkGraph2 {

    // create a random graph

    public vtkRandomGraphSource source;


    static {
        if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
            for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
                if (!lib.IsLoaded())
                    System.out.println(lib.GetLibraryName() + " not loaded");
            }
            System.out.println("Make sure the search path is correct: ");
            System.out.println(System.getProperty("java.library.path"));
        }
        vtkNativeLibrary.DisableOutputWindow(null);
    }

    public static void main(String[] args) {

        //vtkGraph me = new vtkGraph();
        //me.doit();

        vtkGraph2 me2 = new vtkGraph2();


        me2.routine();






    }






    public void routine(){

        source = new vtkRandomGraphSource();


        source.SetNumberOfVertices(2);
        source.SetNumberOfEdges(4);
        source.StartWithTreeOn();
        source.Update();

        //setup a strategy for laying out the graph
        //NOTE:
        // You can set additional options for each strategy, as desired

           vtkFast2DLayoutStrategy strategy =new vtkFast2DLayoutStrategy();

        //#strategy = vtkSimple2DLayoutStrategy()
        //#strategy = vtkCosmicTreeLayoutStrategy()
        //#strategy = vtkForceDirectedLayoutStrategy()
        //#strategy = vtkTreeLayoutStrategy()
        //#set the strategy on the layout


        vtkGraphLayout layout = new vtkGraphLayout();
        layout.SetLayoutStrategy(strategy);
        layout.SetInputConnection(source.GetOutputPort());

        //    #create the renderer to help in sizing glyphs for the vertices


        vtkRenderer ren = new vtkRenderer();
        //#Pipeline for displaying vertices -glyph -> mapper -> actor -> display
        //#mark each vertex with a circle glyph

          vtkGraphToGlyphs vertex_glyphs =new vtkGraphToGlyphs();


        vertex_glyphs.SetInputConnection(layout.GetOutputPort());
        vertex_glyphs.SetGlyphType(6);
        vertex_glyphs.FilledOn();


        vertex_glyphs.SetRenderer(ren);

       // ren.DeviceRender();

        //    #create a mapper for vertex display

        vtkPolyDataMapper vertex_mapper = new vtkPolyDataMapper();
        vertex_mapper.SetInputConnection(vertex_glyphs.GetOutputPort());
        vertex_mapper.SetScalarRange(0, 100);
        vertex_mapper.SetScalarModeToUsePointFieldData();
        vertex_mapper.SelectColorArray("vertex id");

        vertex_mapper.Update();

        //#create the actor for displaying vertices
        vtkActor vertex_actor = new vtkActor();

        vertex_actor.SetMapper(vertex_mapper);

           //Pipeline for displaying edges of the graph - layout -> lines -> mapper-> actor -> display
            //#NOTE:
                //  If no edge layout is performed, all edges will be rendered as
            //#line segments between vertices in the graph.

       // vtkArcParallelEdgeStrategy edge_strategy = new vtkArcParallelEdgeStrategy();

      //  vtkEdgeLayout edge_layout = new vtkEdgeLayout();
       // edge_layout.SetLayoutStrategy(edge_strategy);
      //  edge_layout.SetInputConnection(layout.GetOutputPort());



       // vtkGraphToPolyData edge_geom = new vtkGraphToPolyData();
       // edge_geom.SetInputConnection(edge_layout.GetOutputPort());

        //#create a mapper for edge display

      //  vtkPolyDataMapper edge_mapper = new vtkPolyDataMapper();
      //  edge_mapper.SetInputConnection(edge_geom.GetOutputPort());

        //#create the actor for displaying the edges

      //  vtkActor edge_actor = new vtkActor();
      //  edge_actor.SetMapper(edge_mapper);
       // edge_actor.GetProperty().SetColor(0., 0., 0.);
      //  edge_actor.GetProperty().SetOpacity(0.25);
//

        //vtkRenderer, mapper
        //edge_actor.Render(ren, edge_mapper);

        vertex_glyphs.SetScaling(true);

        vtkRenderer a = new vtkRenderer();
        vtkRenderWindow b = new vtkRenderWindow();

        a.SetBackground(100,100,100);


        b.AddRenderer(a);

        vtkRenderWindowInteractor c = new vtkRenderWindowInteractor();

        c.SetRenderWindow(b);


      //  a.AddActor(edge_actor);

        //vertex_actor.SetScale(.01);



        a.AddActor(vertex_actor);

        b.Render();

        vtkInteractorStyleTrackballCamera d = new vtkInteractorStyleTrackballCamera();

        c.SetInteractorStyle(d);

        c.Start();






    }

}
