package Instrument;

/**
 * Created by Sabatu on 4/19/2017.
 */
public class IntentType {

    public  String ID;
    public  String type;
    public  String caller;
    public  String callsite;
    public  String action;
    public  String datastring;
    public  String datauri;
    public  String scheme;
    public  String flags;
    public  Object vertexID;
    public  String extras;
    public  String component;
    public  String timestamp;
    public String packagename;


    // constructor
    public IntentType(String ID) {
        this.ID = ID;
        this.type = null;
        this.caller = null;
        this.callsite = null;
        this.action = null;
        this.packagename = null;
        this.datastring = null;
        this.datauri = null;
        this.scheme = null;
        this.flags = null;
        this.vertexID = vertexID;
        this.extras = null;
        this.component = null;
        this.timestamp = null;

    }

    // getters
    public  String getID() { return ID; }
    public  String getType() { return type; }
    public  String getCaller() { return caller; }
    public  String getCallsite() { return callsite; }
    public  String getAction() { return action; }
    public  String getPackageName() { return packagename; }
    public  String getdataString() { return datastring; }
    public  String getdataURI() { return datauri; }
    public  String getScheme() { return scheme; }
    public  String getFlags() { return flags; }
    public  Object getvertexID() { return vertexID; }
    public  String getExtras() { return extras; }
    public  String getComponent() { return component; }
    public  String getTimestamp() { return timestamp; }




    // setter

  public void setID(String ID) { this.ID = ID; }
    public void setType(String type) { this.type = type; }
    public void setCaller(String caller) { this.caller = caller; }
    public void setCallsite(String callsite) { this.callsite = callsite; }
    public void setAction(String action) { this.action = action; }
    public void setPackageName(String packagename) { this.packagename = packagename; }
    public void setdataString(String datastring) { this.datastring = datastring; }
    public void setdataURI(String datauri) { this.datauri = datauri; }
    public void setScheme(String scheme) { this.scheme = scheme; }
    public void setFlags(String flags) { this.flags = flags; }
    public void setvertexID(Object vertexID) { this.vertexID = vertexID; }
    public void setExtras(String extras) { this.extras = extras; }
    public void setComponent(String component) { this.component = component; }
    public void setTimestamp(String timestamp) { this.timestamp = timestamp; }


   //resets the class
    public void clear(){

        this.ID = null;
        this.type = null;
        this.caller = null;
        this.callsite = null;
        this.action = null;
        this.packagename = null;
        this.datastring = null;
        this.datauri = null;
        this.scheme = null;
        this.flags = null;
        this.vertexID = vertexID;
        this.extras = null;
        this.component = null;
        this.timestamp = null;
    }
}





