package Instrument;

import android.content.Intent;
import soot.Scene;
import soot.SootClass;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Sabatu on 3/11/2017.
 */
public class output {

    /** Used to avoid infinite recursion */
    private static boolean active = false;

    private static logicClock g_lgclock = null;
    public static void installClock (logicClock lgclock) {
        g_lgclock = lgclock;
    }

    public synchronized static void dumpIntentInfo(Intent itn, String caller, String callsite,String directComp,String ClassComp, String API) {
        if (active) return;
        active = true;
        try {
            dumpIntentInfo_im(itn, caller, callsite, directComp, ClassComp, API);
        } finally {
            active = false;
        }
    }

    public synchronized static void dumpIntentInfo_im(Intent capturedIntent, String method, String callsite,String directComp,String ClassComp, String API) {
        try {

            android.util.Log.e("INTENT TYPE: ", API);

            android.util.Log.e("INTENT CALLER: ", method);

            android.util.Log.e("Component Type of caller: ", ClassComp);

            android.util.Log.e("INTENT CALLSITE: ", callsite);

            android.util.Log.e("Direct Component Type: ", directComp);

            dumpIntent(capturedIntent);

            if (g_lgclock != null) {
                g_lgclock.packClock(capturedIntent);
            }








        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public synchronized static void dumpIntent(Intent itn) {
		/*
		android.util.Log.e("hcai-intent-monitor", "============= Start ===========");
		android.util.Log.e("hcai-intent-monitor", "\tAction="+itn.getAction());
		android.util.Log.e("hcai-intent-monitor", "\tCategories="+itn.getCategories().size());
		for (String cat : itn.getCategories()) {
			android.util.Log.e("hcai-intent-monitor", "\t\t"+cat);
		}
		android.util.Log.e("hcai-intent-monitor", "\tPackageName="+itn.getPackage());
		android.util.Log.e("hcai-intent-monitor", "\tDataString=" + itn.getDataString());
		android.util.Log.e("hcai-intent-monitor", "\tDataURI=" + itn.getData());
		android.util.Log.e("hcai-intent-monitor", "\tScheme=" + itn.getScheme());
		android.util.Log.e("hcai-intent-monitor", "\tFlags=" + itn.getFlags());
		android.util.Log.e("hcai-intent-monitor", "\tType=" + itn.getType());
		android.util.Log.e("hcai-intent-monitor", "\tExtras=" + itn.getExtras());
		android.util.Log.e("hcai-intent-monitor", "\tComponent=" + itn.getComponent());
		android.util.Log.e("hcai-intent-monitor", "============= End ===========");
		*/

        String s = "";
        //s += "============= Start ===========\n";
        try {s += "\tAction="+itn.getAction()+"\n";} catch (Exception e) {}
        try {s += "\tCategories="+itn.getCategories().size()+"\n";} catch (Exception e) {}
        try {
            for (String cat : itn.getCategories()) {
                s += "\t\t"+cat+"\n";
            }
        } catch (Exception e) {}
        try {s += "\tPackageName="+itn.getPackage()+"\n";} catch (Exception e) {}
        try {s += "\tDataString=" + itn.getDataString()+"\n";} catch (Exception e) {}
        try {s += "\tDataURI=" + itn.getData()+"\n";} catch (Exception e) {}
        try {s += "\tScheme=" + itn.getScheme()+"\n";} catch (Exception e) {}
        try {s += "\tFlags=" + itn.getFlags()+"\n";} catch (Exception e) {}
        try {s += "\tType=" + itn.getType()+"\n";} catch (Exception e) {}
        try {s += "\tExtras=" + itn.getExtras()+"\n";} catch (Exception e) {}
        try {s += "\tComponent=" + itn.getComponent()+"\n\n";} catch (Exception e) {}



        //s += "============= End ==========="+"\n";

        android.util.Log.println(0, "Verbose Intent Info", s);
        android.util.Log.e("Verbose Intent Info", s);
    }



    public synchronized static void onBuildInvokedIntent( String invokes) {
        try {





            android.util.Log.e("SUPPLEMENTARY CALL: Intent invoking: ", invokes);


            /*
            //Checks whether there are unresolved variables passed
            if(!args.isEmpty()) {

                Iterator<String> argsIT = args.iterator();

                //Iterates through passed in arguments
                while(argsIT.hasNext()) {

                    String currentArg = argsIT.next();
                    android.util.Log.e(tag, "Unresolved variables: " + currentArg);
                    intentLine++;
                }
            }

            */


        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public synchronized static void onBuildPassedIntent(String passedby) {
        try {


            android.util.Log.e("SUPPLEMENTARY CALL: Intent being passed by: ",  passedby);


            /*

            //Checks whether there are unresolved variables passed
            if(!args.isEmpty()) {

                Iterator<String> argsIT = args.iterator();

                //Iterates through passed in arguments
                while(argsIT.hasNext()) {

                    String currentArg = argsIT.next();
                    android.util.Log.e(tag, "Unresolved variables: " + currentArg);
                    intentLine++;
                }
            }

            */


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void printrawStmt(String stmt) {

        try {





            android.util.Log.e("SUPPLEMENTARY - STATEMENT CALL: ", stmt);



            /*
            //Checks whether there are unresolved variables passed
            if(!args.isEmpty()) {

                Iterator<String> argsIT = args.iterator();

                //Iterates through passed in arguments
                while(argsIT.hasNext()) {

                    String currentArg = argsIT.next();
                    android.util.Log.e(tag, "Unresolved variables: " + currentArg);
                    intentLine++;
                }
            }

            */


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void onSendCallingIntent(String intenttype, String invoking) {

        try {



            android.util.Log.e("[Start of Capture] Intent Monitor --", "[" + intenttype + "]");

            android.util.Log.e("Intent invokes: ", invoking);



            /*
            //Checks whether there are unresolved variables passed
            if(!args.isEmpty()) {

                Iterator<String> argsIT = args.iterator();

                //Iterates through passed in arguments
                while(argsIT.hasNext()) {

                    String currentArg = argsIT.next();
                    android.util.Log.e(tag, "Unresolved variables: " + currentArg);
                    intentLine++;
                }
            }

            */


        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public synchronized static void onRecvCallingIntent(String intenttype, String invoking) {
        try {



            android.util.Log.e("[Start of Capture] Intent Monitor --", "[" + intenttype + "]");

            android.util.Log.e("Intent invokes: ",  invoking);

            /*

            //Checks whether there are unresolved variables passed
            if(!args.isEmpty()) {

                Iterator<String> argsIT = args.iterator();

                //Iterates through passed in arguments
                while(argsIT.hasNext()) {

                    String currentArg = argsIT.next();
                    android.util.Log.e(tag, "Unresolved variables: " + currentArg);
                    intentLine++;
                }
            }

            */


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void onSendPassedIntent(String intenttype, String passedby) {
        try {



            android.util.Log.e("[Start of Capture] Intent Monitor --", "[" + intenttype + "]");

            android.util.Log.e("Intent being passed by: ",  passedby);

            /*

            //Checks whether there are unresolved variables passed
            if(!args.isEmpty()) {

                Iterator<String> argsIT = args.iterator();

                //Iterates through passed in arguments
                while(argsIT.hasNext()) {

                    String currentArg = argsIT.next();
                    android.util.Log.e(tag, "Unresolved variables: " + currentArg);
                    intentLine++;
                }
            }

            */


        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public synchronized static void onRecvPassedIntent(String intenttype, String passedby) {
        try {



            android.util.Log.e("[Start of Capture] Intent Monitor --", "[" + intenttype + "]" );

            android.util.Log.e("Intent being passed by: ",  passedby);


            /*

            //Checks whether there are unresolved variables passed
            if(!args.isEmpty()) {

                Iterator<String> argsIT = args.iterator();

                //Iterates through passed in arguments
                while(argsIT.hasNext()) {

                    String currentArg = argsIT.next();
                    android.util.Log.e(tag, "Unresolved variables: " + currentArg);
                    intentLine++;
                }
            }

            */

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public synchronized static void supplementaryInvoke(String supplement) {
        try {

            android.util.Log.e("Intent invokes: ",  supplement);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void supplementaryCall(String supplement) {
        try {

            android.util.Log.e("Intent being passed by: ",  supplement);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }




        } //<===Class instance










