package Instrument;

/**
 * Created by Sabatu on 4/19/2017.
 */

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;


import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Created by Sabatu on 4/5/2017.
 */

public class dynCGvisualizer extends JApplet {


    public static mxGraph graph;
    public static Object parent;
    public IntentType[] intentStorage = new IntentType[200];
    public int intentCounter = 0;
    public IntentType intents;

    //beginning of class direction
    public dynCGvisualizer() {




        // create an mxGraph graph

        graph = new mxGraph();
        parent = graph.getDefaultParent();

        final mxGraphComponent graphComponent = new mxGraphComponent(graph);
        getContentPane().add(graphComponent);

        graphComponent.getGraphControl().addMouseListener(new MouseAdapter()
        {

            public void mouseReleased(MouseEvent e)
            {
                Object cell = graphComponent.getCellAt(e.getX(), e.getY());

                if (cell != null)
                {
                    IntentType selectedIntent;


                    String vertexLabel = graph.getLabel(cell);

                    selectedIntent = findIntent(vertexLabel);

                    if(selectedIntent.getID() != "0")
                    {

                        postIntentInfo(selectedIntent);

                    }



                }
            }
        });





    }

    public void postIntentInfo(IntentType selectedIntent)
    {

        String windowOutput = new String();

        if(selectedIntent.getAction() != null)
        {
            windowOutput += "Action: " + selectedIntent.getAction() + "\n";
        }

        if(selectedIntent.getCaller() != null)
        {
            windowOutput += "Caller: " + selectedIntent.getCaller() + "\n";
        }

        if(selectedIntent.getCallsite() != null)
        {
            windowOutput += "Callsite: " + selectedIntent.getCallsite() + "\n";
        }

        if(selectedIntent.getComponent() != null)
        {
            windowOutput += "Component: " + selectedIntent.getComponent() + "\n";
        }

        if(selectedIntent.getdataString() != null)
        {
            windowOutput += "Data String: " + selectedIntent.getdataString() + "\n";
        }

        if(selectedIntent.getdataURI() != null)
        {
            windowOutput += "Data URI: " + selectedIntent.getdataURI() + "\n";
        }

        if(selectedIntent.getExtras() != null)
        {
            windowOutput += "Extras: " + selectedIntent.getExtras() + "\n";
        }

        if(selectedIntent.getFlags() != null)
        {
            windowOutput += "Flags: " + selectedIntent.getFlags() + "\n";
        }

        if(selectedIntent.getID() != null)
        {
            windowOutput += "Line number: " + selectedIntent.getID() + "\n";
        }

        if(selectedIntent.getPackageName() != null)
        {
            windowOutput += "Package Name: " + selectedIntent.getPackageName() + "\n";
        }

        if(selectedIntent.getScheme() != null)
        {
            windowOutput += "Scheme: " + selectedIntent.getScheme() + "\n";
        }

        if(selectedIntent.getTimestamp() != null)
        {
            windowOutput += "Timestamp: " + selectedIntent.getTimestamp() + "\n";
        }

        if(selectedIntent.getType() != null)
        {
            windowOutput += "Type: " + selectedIntent.getType() + "\n";
        }

        Dimension DEFAULT_SIZE = new Dimension( 400, 200 );

        JTextArea textarea = new JTextArea(10,50);
        textarea.setText(windowOutput);
        //getContentPane().add(textarea, BorderLayout.LINE_START);
       // getContentPane().setVisible(true);

        JFrame frame2 = new JFrame();
        frame2.getContentPane().add(textarea, BorderLayout.LINE_START);

        frame2.resize(DEFAULT_SIZE );
        frame2.setVisible(true);


        System.out.println("This is also a test.");

    }

    public IntentType findIntent(String matchStatement)
    {
        for( int i = 0; i < intentCounter ; i++)
        {




            //intentType.getType() + " at line " + intentType.getID();

            String comparisonString = intentStorage[i].getType() + " at line " + intentStorage[i].getID();

            System.out.println("THE INPUT INTENT ID IS: " + intentStorage[i].getID());

            System.out.println("The intent counter is: " + intentCounter);

            System.out.println("comparison statement: " + comparisonString);
            System.out.println("match statement: " + matchStatement);
            System.out.println();



            if(comparisonString.contentEquals(matchStatement))
            {
                System.out.println("This returned something?");
                return intentStorage[i];
            }
        }

        IntentType noReturn = new IntentType("1");

        return noReturn;

    }

    //updateGraph inserts next vertexes..
    public void updateGraph(IntentType[] intentType,int index, int x, int y) {

       intentStorage[intentCounter] = intentType[index];




        graph.getModel().beginUpdate();





        try
        {

            /*In order: 1.) parent cell.
                        2.) id – this is a global unique identifier that describes the cell, it is always a string.
                        3.) value – this is the user object of the cell. User object are simply that, just objects,
                         but form the objects that allow you to associate the business logic of an application with
                         the visual representation of JGraphX. They will be described in more detail later in this manual,
                         however, to start with if you use a string as the user object, this will be displayed as the label on the vertex or edge.
                        4.) Numbers: x, y, width, height
                        5.) style – the style description to be applied to this vertex.


             */



            //intents.setID(saveMe);

            String label = intentType[index].getType() + " at line " + intentType[index].getID();
            Object v1 = graph.insertVertex(parent, null, label, x, y, 120,40, "shape=ellipse;whiteSpace=wrap;strokeColor=black;fillColor=white");

            intentStorage[intentCounter].setvertexID(v1);

            if(intentCounter > 0)
            {
                IntentType comparison = intentStorage[intentCounter - 1];

                if(comparison.getComponent() == intentStorage[intentCounter].getComponent() && (comparison.getType() != intentStorage[intentCounter].getType()))
                {
                    graph.insertEdge(parent,null,intentStorage[intentCounter].getComponent(),comparison.getvertexID(),v1);
                }

                if(comparison.getAction() == intentStorage[intentCounter].getAction() && (comparison.getType() != intentStorage[intentCounter].getType()))
                {
                    graph.insertEdge(parent,null,intentStorage[intentCounter].getAction(),comparison.getvertexID(),v1);
                }

              if(comparison.getdataString() == intentStorage[intentCounter].getdataString() && (comparison.getType() != intentStorage[intentCounter].getType()))
               {
                    graph.insertEdge(parent,null,intentStorage[intentCounter].getdataString(),comparison.getvertexID(),v1);
                }

                if(comparison.getdataURI() == intentStorage[intentCounter].getdataURI() && (comparison.getType() != intentStorage[intentCounter].getType()))
               {
                   graph.insertEdge(parent,null,intentStorage[intentCounter].getdataURI(),comparison.getvertexID(),v1);
               }
            }



            //graph.insertEdge(parent, null, "Edge", v1, v2);
        }
        finally
        {
            graph.getModel().endUpdate();
        }

        mxGraphComponent graphComponent = new mxGraphComponent(graph);
        getContentPane().add(graphComponent);

        intentCounter += 1;




    }



    //Starts a swing thread, passes it a new instance of dynCGvisualizer
    public void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                //dynCGvisualizer frame = new dynCGvisualizer();
                //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
              // frame.setSize(400, 320);
               // frame.setVisible(true);

            }
        });


    } //<===main method

} //<===public class flipper